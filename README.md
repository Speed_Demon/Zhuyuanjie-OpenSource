关于该Tomcat核心源码仿写

我在CSDN中做了详细介绍

请查看 https://blog.csdn.net/Fearless____/article/details/131445635?fromshare=blogdetail



2023/08/17 迭代出第二版代码，在第一版的基础上进行如下改进：

- 升级为Maven项目
- 将单线程改为多线程，并且可通过配置文件配置线程池
- 加入日志打印
- 优化代码排版，大大提高可读性