package myTomcat.myHttpRequestAndHttpResponse;

import java.io.OutputStream;

public class MyHttpResponse {

    private OutputStream outputStream;

    public static final String responseLineAndHead = "HTTP/1.1 200+\r\n" + "Content-Type：text/html+\r\n"
            + "\r\n";

    public MyHttpResponse(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }
}
