package myTomcat.myHttpRequestAndHttpResponse;

import java.util.HashMap;

public class MyHttpRequest {

    private HashMap<String, String> map = new HashMap<>();

    public String getParameter(String key) {
        return map.get(key);
    }

    public HashMap<String, String> getMap() {
        return map;
    }

    public void setMap(HashMap<String, String> map) {
        this.map = map;
    }
}