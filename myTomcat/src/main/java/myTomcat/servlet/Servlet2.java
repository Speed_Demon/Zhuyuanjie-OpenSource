package myTomcat.servlet;

import myTomcat.myHttpRequestAndHttpResponse.MyHttpRequest;
import myTomcat.myHttpRequestAndHttpResponse.MyHttpResponse;
import myTomcat.myServletAnnotation.MyServlet;

import java.io.IOException;

@MyServlet(path = "address2")
public class Servlet2 {
    public void doGet(MyHttpRequest request, MyHttpResponse response) throws IOException {
        System.out.println("address2 GET响应：");
        System.out.println("a=" + request.getParameter("a"));
        System.out.println("\n响应的http如下：");
        String resp = MyHttpResponse.responseLineAndHead + "[\r\n" +
                "{\"id\":\"1\",\"name\":\"ssh\",\"sex\":\"女\",\"age\":\"21\"},\r\n" +
                "{\"id\":\"1\",\"name\":\"ssh\",\"sex\":\"女\",\"age\":\"20\"},\r\n" +
                "{\"id\":\"1\",\"name\":\"ssh\",\"sex\":\"女\",\"age\":\"23\"},\r\n" +
                "{\"id\":\"1\",\"name\":\"ssh\",\"sex\":\"女\",\"age\":\"21\"}\r\n" +
                "]\r\n" +
                "";
        System.out.println(resp);
        response.getOutputStream().write(resp.getBytes());
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }

    public void doPost(MyHttpRequest request, MyHttpResponse response) throws IOException {
        doGet(request,response);
    }
}
