package myTomcat.servlet;

import myTomcat.myHttpRequestAndHttpResponse.MyHttpRequest;
import myTomcat.myHttpRequestAndHttpResponse.MyHttpResponse;
import myTomcat.myServletAnnotation.MyServlet;

import java.io.IOException;

@MyServlet(path = "address1")
public class Servlet1 {

    public void doGet(MyHttpRequest request, MyHttpResponse response) throws IOException {
        System.out.println("address1 GET响应：");
        System.out.println("获取参数 a=" + request.getParameter("a"));
        System.out.println("\n响应的http如下：");
        String resp = MyHttpResponse.responseLineAndHead + "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <meta charset=\"utf-8\" />\n" +
                "</head>\n" +
                "<body>\n" +
                " \n" +
                "    <form name=\"my_form\" method=\"POST\">\n" +
                "        <input type=\"button\" value=\"按下\" onclick=\"alert('你按下了按钮')\">\n" +
                "    </form>\n" +
                " \n" +
                "</body>\n" +
                "</html>";
        System.out.println(resp);
        response.getOutputStream().write(resp.getBytes());
        response.getOutputStream().flush();
        response.getOutputStream().close();

    }

    public void doPost(MyHttpRequest request, MyHttpResponse response) throws IOException {
        doGet(request,response);
    }

}
